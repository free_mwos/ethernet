Async Ethernet "2" (W5500) Library for MWOS
===================================

Fork Arduino EthernetLibrary "2" with async connect
https://gitlab.com/free_mwos/ethernet


W5500 Shields
-------------

* [Adafruit W5500 Ethernet Shield](https://www.adafruit.com/products/2971)
* [Arduino Ethernet Shield v2](https://www.arduino.cc/en/Main/ArduinoEthernetShieldV2)
* [Industruino Ethernet module](https://industruino.com/shop/product/ethernet-expansion-module-10)
* [Wiznet W5500 Ethernet Shield](http://www.wiznet.co.kr/product-item/w5500-ethernet-shield/)

* [Ethernet "2" Library for Arduino] (https://github.com/adafruit/Ethernet2)

